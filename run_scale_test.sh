#!/bin/sh

management_tool="kubernetes"
replicas=2
count=50
delay=45
timeout=180000

# Copy test script to master
ansible master -b -m copy -a "src=scale_app.sh dest=/tmp/scale_app.sh mode=700"

if [ "$management_tool" = "kubernetes" ]
then
    # Prepare test
    ansible-playbook "$management_tool"/site.yml -t init,storage

    # Deploy app 1
    ansible-playbook "$management_tool"/site.yml -t "static-website" -e replicas=2

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n deployment/static-website -s static-website -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "static-website"

    # Deploy app 2
    ansible-playbook "$management_tool"/site.yml -t "wordpress" -e replicas=2

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n deployment/wordpress -s wordpress -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "wordpress"

    # Deploy app 3
    ansible-playbook "$management_tool"/site.yml -t "rocketchat" -e replicas=2

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n deployment/rocketchat -s rocketchat -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "rocketchat"
elif [ "$management_tool" = "swarm" ]
then
    ansible-playbook "$management_tool"/site.yml -t init,storage

    # Deploy app 1
    ansible-playbook "$management_tool"/site.yml -t "static-website" -e replicas=3

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n website -s website -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "static-website"

    # Deploy app 2
    ansible-playbook "$management_tool"/site.yml -t "wordpress" -e replicas=3

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n wordpress -s wp -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "wordpress"

    # Deploy app 3
    ansible-playbook "$management_tool"/site.yml -t "chat" -e replicas=3

    # Run test script
    ansible master -B "$timeout" -P 10 -b -m command -a "/tmp/scale_app.sh -v -m $management_tool -n rocketchat -s chat -c $count -d $delay -r $replicas"

    # Reset site
    ansible-playbook "$management_tool"/reset_site.yml -t "chat"
else
    echo "Unknown management_tool $management_tool"
    exit 1
fi

