#read in data
library(readr)
for(num in 1:100){
  assign(sprintf("prom_static-website_swarm3_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_swarm/prom_static-website_swarm_%i.csv",num)))
  assign(sprintf("static-website_swarm3_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_swarm/static-website_swarm_%i.csv",num)))
  assign(sprintf("prom_static-website_kubernetes3_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s/prom_static-website_kubernetes_%i.csv",num)))
  assign(sprintf("static-website_kubernetes3_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s/static-website_kubernetes_%i.csv",num)))
  assign(sprintf("prom_static-website_kubernetes3cd_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s_containerd/prom_static-website_kubernetes_%i.csv",num)))
  assign(sprintf("static-website_kubernetes3cd_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s_containerd/static-website_kubernetes_%i.csv",num)))
  assign(sprintf("prom_static-website_kubernetes3cr_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s_crio/prom_static-website_3rep_kubernetes_%i.csv",num)))
  assign(sprintf("static-website_kubernetes3cr_%i.csv",num),read_csv(sprintf("Rdata/static_website_normal_3rep_k8s_crio/static-website_3rep_kubernetes_%i.csv",num)))
}

#Calculate kubernetes avg runtime,memusage,cpuusage,networkusage & error rate
library(rowr)
time_taken <- sum(`static-website_kubernetes3_1.csv`$elapsed)
errorrate <- colSums(!t(`static-website_kubernetes3_1.csv`[,8]),na.rm = TRUE)
memusage <- data.frame(`prom_static-website_kubernetes3_1.csv`[,2])
std <- data.frame(`static-website_kubernetes3_1.csv`[grepl("TRUE",`static-website_kubernetes3_1.csv`$success),2])
dropped <- data.frame(`prom_static-website_kubernetes3_1.csv`[,3])
cpuusage <- data.frame(`prom_static-website_kubernetes3_1.csv`[,5])
networkusage <- data.frame(`prom_static-website_kubernetes3_1.csv`[,6])
for(num in 2:100){
  time_taken <- time_taken+ sum(get(sprintf("static-website_kubernetes3_%i.csv",num))$elapsed)
  std <- cbind.fill(std,get(sprintf("static-website_kubernetes3_%i.csv",num))[grepl("TRUE",get(sprintf("static-website_kubernetes3_%i.csv",num))$success),2],fill = NA)
  errorrate <- errorrate+colSums(!t(get(sprintf("static-website_kubernetes3_%i.csv",num))[,8]),na.rm = TRUE)
  memusage <- cbind.fill(memusage,get(sprintf("prom_static-website_kubernetes3_%i.csv",num))[,2],fill = NA)
  dropped <- cbind.fill(dropped,get(sprintf("prom_static-website_kubernetes3_%i.csv",num))[,3],fill = NA)
  cpuusage <- cbind.fill(cpuusage,get(sprintf("prom_static-website_kubernetes3_%i.csv",num))[,5],fill = NA)
  networkusage <- cbind.fill(networkusage,get(sprintf("prom_static-website_kubernetes3_%i.csv",num))[,6],fill = NA)
}
std <- data.matrix(std)
std_kub <- std
sd_kub <- sd(std,na.rm=TRUE)
avg_memusage <- rowSums(memusage)/100000000
errorrate_kubernetes <- errorrate
avg_time_taken_kubernetes <- time_taken/100
avg_dropped <- rowSums(dropped)/100
avg_cpuusage <- rowSums(cpuusage)/100
avg_networkusage <- rowSums(networkusage)/100000

#Calculate kubernetes avg runtime,memusage,cpuusage,networkusage & error rate
library(rowr)
time_taken <- sum(`static-website_kubernetes3cd_1.csv`$elapsed)
errorrate <- colSums(!t(`static-website_kubernetes3cd_1.csv`[,8]),na.rm = TRUE)
std <- data.frame(`static-website_kubernetes3cd_1.csv`[grepl("TRUE",`static-website_kubernetes3cd_1.csv`$success),2])
memusage <- data.frame(`prom_static-website_kubernetes3cd_1.csv`[,2])
dropped <- data.frame(`prom_static-website_kubernetes3cd_1.csv`[,3])
cpuusage <- data.frame(`prom_static-website_kubernetes3cd_1.csv`[,5])
networkusage <- data.frame(`prom_static-website_kubernetes3cd_1.csv`[,6])
for(num in 2:100){
  time_taken <- time_taken+ sum(get(sprintf("static-website_kubernetes3cd_%i.csv",num))$elapsed)
  std <- cbind.fill(std,get(sprintf("static-website_kubernetes3cd_%i.csv",num))[grepl("TRUE",get(sprintf("static-website_kubernetes3cd_%i.csv",num))$success),2],fill = NA)
  errorrate <- errorrate+colSums(!t(get(sprintf("static-website_kubernetes3cd_%i.csv",num))[,8]),na.rm = TRUE)
  memusage <- cbind.fill(memusage,get(sprintf("prom_static-website_kubernetes3cd_%i.csv",num))[,2],fill = NA)
  dropped <- cbind.fill(dropped,get(sprintf("prom_static-website_kubernetes3cd_%i.csv",num))[,3],fill = NA)
  cpuusage <- cbind.fill(cpuusage,get(sprintf("prom_static-website_kubernetes3cd_%i.csv",num))[,5],fill = NA)
  networkusage <- cbind.fill(networkusage,get(sprintf("prom_static-website_kubernetes3cd_%i.csv",num))[,6],fill = NA)
}
std <- data.matrix(std)
std_kub_cd <- std
sd_kub_cd <- sd(std,na.rm=TRUE)
avg_memusage_cd <- rowSums(memusage)/100000000
errorrate_kubernetes_cd <- errorrate
avg_time_taken_cd <- time_taken/100
avg_dropped_cd <- rowSums(dropped)/100
avg_cpuusage_cd <- rowSums(cpuusage)/100
avg_networkusage_cd <- rowSums(networkusage)/100000


#Calculate kubernetes avg runtime,memusage,cpuusage,networkusage & error rate
library(rowr)
time_taken <- sum(`static-website_kubernetes3cr_1.csv`$elapsed)
errorrate<-colSums(!t(`static-website_kubernetes3cr_1.csv`[,8]),na.rm = TRUE)
memusage <- data.frame(`prom_static-website_kubernetes3cr_1.csv`[,2])
std <- data.frame(`static-website_kubernetes3cr_1.csv`[grepl("TRUE",`static-website_kubernetes3cr_1.csv`$success),2])
dropped <- data.frame(`prom_static-website_kubernetes3cr_1.csv`[,3])
cpuusage <- data.frame(`prom_static-website_kubernetes3cr_1.csv`[,5])
networkusage <- data.frame(`prom_static-website_kubernetes3cr_1.csv`[,6])
for(num in 2:100){
  time_taken <- time_taken+ sum(get(sprintf("static-website_kubernetes3cr_%i.csv",num))$elapsed)
  std <- cbind.fill(std,get(sprintf("static-website_kubernetes3cr_%i.csv",num))[grepl("TRUE",get(sprintf("static-website_kubernetes3cr_%i.csv",num))$success),2],fill = NA)
  errorrate <- errorrate+colSums(!t(get(sprintf("static-website_kubernetes3cr_%i.csv",num))[,8]),na.rm = TRUE)
  memusage <- cbind.fill(memusage,get(sprintf("prom_static-website_kubernetes3cr_%i.csv",num))[,2],fill = NA)
  dropped <- cbind.fill(dropped,get(sprintf("prom_static-website_kubernetes3cr_%i.csv",num))[,3],fill = NA)
  cpuusage <- cbind.fill(cpuusage,get(sprintf("prom_static-website_kubernetes3cr_%i.csv",num))[,5],fill = NA)
  networkusage <- cbind.fill(networkusage,get(sprintf("prom_static-website_kubernetes3cr_%i.csv",num))[,6],fill = NA)
}
std <- data.matrix(std)
std_kub_cr <- std
sd_kub_cr <- sd(std,na.rm=TRUE)
avg_memusage_cr <- rowSums(memusage)/100000000
errorrate_kubernetes_cr <- errorrate
avg_time_taken_cr <- time_taken/100
#avg_dropped_cr <- rowSums(dropped)/100
#avg_cpuusage_cr <- rowSums(na.omit(cpuusage))/100
#avg_networkusage_cr <- rowSums(na.omit(networkusage))/100000


#Calculate swarm avg runtime & error rate
library(rowr)
time_taken <- sum(`static-website_swarm3_1.csv`$elapsed)
errorrate<-colSums(!t(`static-website_swarm3_1.csv`[,8]),na.rm = TRUE)
memusage <- data.frame(`prom_static-website_swarm3_1.csv`[,2])
std <- data.frame(`static-website_swarm3_1.csv`[grepl("TRUE",`static-website_swarm3_1.csv`$success),2])
dropped <- data.frame(`prom_static-website_swarm3_1.csv`[,3])
cpuusage <- data.frame(`prom_static-website_swarm3_1.csv`[,5])
networkusage <- data.frame(`prom_static-website_swarm3_1.csv`[,6])
for(num in 2:100){
  time_taken <- time_taken+ sum(get(sprintf("static-website_swarm3_%i.csv",num))$elapsed)
  std <- cbind.fill(std,get(sprintf("static-website_swarm3_%i.csv",num))[grepl("TRUE",get(sprintf("static-website_swarm3_%i.csv",num))$success),2],fill = NA)
  errorrate <- errorrate+colSums(!t(get(sprintf("static-website_swarm3_%i.csv",num))[,8]),na.rm = TRUE)
  memusage <- cbind.fill(memusage,get(sprintf("prom_static-website_swarm3_%i.csv",num))[,2],fill = NA)
  dropped <- cbind.fill(dropped,get(sprintf("prom_static-website_swarm3_%i.csv",num))[,3],fill = NA)
  cpuusage <- cbind.fill(cpuusage,get(sprintf("prom_static-website_swarm3_%i.csv",num))[,5],fill = NA)
  networkusage <- cbind.fill(networkusage,get(sprintf("prom_static-website_swarm3_%i.csv",num))[,6],fill = NA)
}
std <- data.matrix(std)
std_swarm <- std
sd_swarm <- sd(std,na.rm=TRUE)
avg_memusage_swarm <- rowSums(memusage)/100000000
errorrate_swarm <- errorrate
avg_time_taken_swarm <- time_taken/100
avg_dropped_swarm <- rowSums(dropped)/100
avg_cpuusage_swarm <- rowSums(cpuusage)/100
avg_networkusage_swarm <- rowSums(networkusage)/100000


# Seperate swarm runtimes into buckets for barplot
buckets <- data.frame(matrix(0,ncol=10,nrow=1))
colnames(buckets) <- c("0-9","10","11","12","13-15","16-17","18-20","21-23","24+","Error")
for(num in 1:100){
  tmp <- get(sprintf("static-website_swarm3_%i.csv",num))[,2]
  response <- get(sprintf("static-website_swarm3_%i.csv",num))[,4]
  threads <- get(sprintf("static-website_swarm3_%i.csv",num))[,6]
  buckets$`Error`<- buckets$`Error`+length(t(tmp[!grepl("200",response$responseCode),]))
  buckets$`0-9`<- buckets$`0-9`+length(t(tmp[tmp<10,]))
  buckets$`10`<- buckets$`10`+length(t(tmp[10==tmp,]))
  buckets$`11`<- buckets$`11`+length(t(tmp[11==tmp,]))
  buckets$`12`<- buckets$`12`+length(t(tmp[tmp==12,]))
  buckets$`13-15`<- buckets$`13-15`+length(t(tmp[13<=tmp&tmp<=15,]))
  buckets$`16-17`<- buckets$`16-17`+length(t(tmp[16<=tmp&tmp<=17,]))
  buckets$`18-20`<- buckets$`18-20`+length(t(tmp[18<=tmp&tmp<=20,]))
  buckets$`21-23`<- buckets$`21-23`+length(t(tmp[21<=tmp&tmp<=23,]))
  buckets$`24+`<- buckets$`24+`+length(t(tmp[24<=tmp,]))

}
df <- data.frame(buckets)


# Seperate kubernetes runtimes into buckets for barplot
buckets <- data.frame(matrix(0,ncol=10,nrow=1))
colnames(buckets) <- c("0-9","10","11","12","13-15","16-17","18-20","21-23","24+","Error")
for(num in 1:100){
  tmp <- get(sprintf("static-website_kubernetes3_%i.csv",num))[,2]
  response <- get(sprintf("static-website_kubernetes3_%i.csv",num))[,4]
  threads <- get(sprintf("static-website_kubernetes3_%i.csv",num))[,6]
  buckets$`Error`<- buckets$`Error`+length(t(tmp[  !grepl("200",response$responseCode),]))
  buckets$`0-9`<- buckets$`0-9`+length(t(tmp[tmp<10,]))
  buckets$`10`<- buckets$`10`+length(t(tmp[10==tmp,]))
  buckets$`11`<- buckets$`11`+length(t(tmp[11==tmp,]))
  buckets$`12`<- buckets$`12`+length(t(tmp[tmp==12,]))
  buckets$`13-15`<- buckets$`13-15`+length(t(tmp[13<=tmp&tmp<=15,]))
  buckets$`16-17`<- buckets$`16-17`+length(t(tmp[16<=tmp&tmp<=17,]))
  buckets$`18-20`<- buckets$`18-20`+length(t(tmp[18<=tmp&tmp<=20,]))
  buckets$`21-23`<- buckets$`21-23`+length(t(tmp[21<=tmp&tmp<=23,]))
  buckets$`24+`<- buckets$`24+`+length(t(tmp[24<=tmp,]))
}
colnames(df)<-colnames(buckets) 
df <- rbind(df,buckets)

# Seperate kubernetes runtimes into buckets for barplot
buckets <- data.frame(matrix(0,ncol=10,nrow=1))
colnames(buckets) <- c("0-9","10","11","12","13-15","16-17","18-20","21-23","24+","Error")
for(num in 1:100){
  tmp <- get(sprintf("static-website_kubernetes3cd_%i.csv",num))[,2]
  response <- get(sprintf("static-website_kubernetes3cd_%i.csv",num))[,4]
  threads <- get(sprintf("static-website_kubernetes3cd_%i.csv",num))[,6]
  buckets$`Error`<- buckets$`Error`+length(t(tmp[  !grepl("200",response$responseCode),]))
  buckets$`0-9`<- buckets$`0-9`+length(t(tmp[tmp<10,]))
  buckets$`10`<- buckets$`10`+length(t(tmp[10==tmp,]))
  buckets$`11`<- buckets$`11`+length(t(tmp[11==tmp,]))
  buckets$`12`<- buckets$`12`+length(t(tmp[tmp==12,]))
  buckets$`13-15`<- buckets$`13-15`+length(t(tmp[13<=tmp&tmp<=15,]))
  buckets$`16-17`<- buckets$`16-17`+length(t(tmp[16<=tmp&tmp<=17,]))
  buckets$`18-20`<- buckets$`18-20`+length(t(tmp[18<=tmp&tmp<=20,]))
  buckets$`21-23`<- buckets$`21-23`+length(t(tmp[21<=tmp&tmp<=23,]))
  buckets$`24+`<- buckets$`24+`+length(t(tmp[24<=tmp,]))
}
colnames(df)<-colnames(buckets) 
df <- rbind(df,buckets)

# Seperate kubernetes runtimes into buckets for barplot
buckets <- data.frame(matrix(0,ncol=10,nrow=1))
colnames(buckets) <- c("0-9","10","11","12","13-15","16-17","18-20","21-23","24+","Error")
for(num in 1:100){
  tmp <- get(sprintf("static-website_kubernetes3cr_%i.csv",num))[,2]
  response <- get(sprintf("static-website_kubernetes3cr_%i.csv",num))[,4]
  threads <- get(sprintf("static-website_kubernetes3cr_%i.csv",num))[,6]
  buckets$`Error`<- buckets$`Error`+length(t(tmp[  !grepl("200",response$responseCode),]))
  buckets$`0-9`<- buckets$`0-9`+length(t(tmp[tmp<10,]))
  buckets$`10`<- buckets$`10`+length(t(tmp[10==tmp,]))
  buckets$`11`<- buckets$`11`+length(t(tmp[11==tmp,]))
  buckets$`12`<- buckets$`12`+length(t(tmp[tmp==12,]))
  buckets$`13-15`<- buckets$`13-15`+length(t(tmp[13<=tmp&tmp<=15,]))
  buckets$`16-17`<- buckets$`16-17`+length(t(tmp[16<=tmp&tmp<=17,]))
  buckets$`18-20`<- buckets$`18-20`+length(t(tmp[18<=tmp&tmp<=20,]))
  buckets$`21-23`<- buckets$`21-23`+length(t(tmp[21<=tmp&tmp<=23,]))
  buckets$`24+`<- buckets$`24+`+length(t(tmp[24<=tmp,]))
}
colnames(df)<-colnames(buckets) 
df <- rbind(df,buckets)


#Plot comparision diagrams
b<-barplot(as.matrix(as.data.frame(df/1000)),col =c("red","#0BA1E2","#ffcc00","green"),main = "Statische Website 3 Replikas normale Last",xaxt="n",las=2,ylim = c(0,60),beside = TRUE,yaxt="n",xlab="Antwortzeit[ms]",ylab="Anteil in %")
par(mar=c(5,4.5,4,1)+.1)
text(cex=0.8, x=b[1,]+0.25, y=-5, colnames(df), xpd=TRUE, srt=65)
b<-axis(2,at=seq(0,60,by=5),labels=FALSE)
text(cex=0.8,x=-3.6,y=b, b,xpd=TRUE)
legend("topright",col =c("red","#0BA1E2","#ffcc00","green"),legend = c("Swarm","K8s-Docker","K8s-containerd","K8s-cri-o"),lty = 1)


plot(seq(1,length(na.omit(avg_memusage_swarm))*3,3),na.omit(avg_memusage_swarm),main = "Statische Website 3 Replikas normale Last",type="l",col="red",ylab = "Durchschnittlicher RAM-Verbrauch Anwendung [MByte]",xlab = "Laufzeit [s]",ylim = c(0,80))
lines(seq(1,length(na.omit(avg_memusage))*3,3),na.omit(avg_memusage),col="#0BA1E2")
lines(seq(1,length(na.omit(avg_memusage_cd))*3,3),na.omit(avg_memusage_cd),col="#ffcc00")
lines(seq(1,length(na.omit(avg_memusage_cr))*3,3),na.omit(avg_memusage_cr),col="green")
legend("topright",col =c("red","#0BA1E2","#ffcc00","green"),legend = c("Swarm","K8s-Docker","K8s-containerd","K8s-cri-o"),lty = 1)

plot(seq(1,length(t(std_swarm)[1,]),1),colMeans(t(std_swarm)),main = "Statische Website 3 Replikas normale Last",type="l",col="red",ylab = "Durchschnittliche Antwortzeit Testdurchlauf [ms]",xlab = "Anfrage")
lines(seq(1,length(t(std_kub)[1,]),1),colMeans(t(std_kub)),col="#0BA1E2")
lines(seq(1,length(t(std_kub_cd)[1,]),1),colMeans(t(std_kub_cd)),col="#ffcc00")
lines(seq(1,length(t(std_kub_cr)[1,]),1),colMeans(t(std_kub_cr)),col="green")
legend("topright",col =c("red","#0BA1E2","#ffcc00","green"),legend = c("Swarm","K8s-Docker","K8s-containerd","K8s-cri-o"),lty = 1)

plot(seq(1,length(na.omit(avg_cpuusage_swarm))*3,3),na.omit(avg_cpuusage_swarm/0.32),main = "Statische Website 3 Replikas normale Last",type="l",ylab = "Durchschnittliche CPU-Nutzung Anwendung [%]",xlab = "Laufzeit [s]",col="red",ylim = c(0,0.015))
lines(seq(1,length(na.omit(avg_cpuusage))*3,3),na.omit(avg_cpuusage/0.32),col="#0BA1E2")
lines(seq(1,length(na.omit(avg_cpuusage_cd))*3,3),na.omit(avg_cpuusage_cd/0.32),col="#ffcc00")
#lines(seq(1,length(na.omit(avg_cpuusage_cr))*3,3),na.omit(avg_cpuusage_cr),col="green")
legend("topright",col =c("red","#0BA1E2","#ffcc00"),legend = c("Swarm","K8s-Docker","K8s-containerd"),lty = 1)

plot(seq(1,length(na.omit(avg_networkusage_swarm))*3,3),na.omit(avg_networkusage_swarm),main = "Statische Website 3 Replikas normale Last",type="l",ylab = "Durchschnittliche Netzwerk-Nutzung Anwendung [KB/s]",xlab = "Laufzeit [s]",col="red",ylim = c(0,15))
lines(seq(1,length(na.omit(avg_networkusage))*3,3),na.omit(avg_networkusage),col="#0BA1E2")
lines(seq(1,length(na.omit(avg_networkusage_cd))*3,3),na.omit(avg_networkusage_cd),col="#ffcc00")
legend("topright",col =c("red","#0BA1E2","#ffcc00"),legend = c("Swarm","K8s-Docker","K8s-containerd"),lty = 1)

plot(errorrate_swarm,type="l",col="red",ylim = c(0,15),main = "Statische Website 3 Replikas normale Last",xlab="Anfragen",ylab="Fehlerrate [%]",yaxt="n")
lines(errorrate_kubernetes,col="#0BA1E2")
lines(errorrate_kubernetes_cd,col="#ffcc00")
lines(errorrate_kubernetes_cr,col="green")
legend("topright",col =c("red","#0BA1E2","#ffcc00","green"),legend = c("Swarm","K8s-Docker","K8s-containerd","K8s-cri-o"),lty = 1)
ticks<-seq(0,15,3)
axis(2,at=ticks,labels=ticks)

avg_time_taken_swarm/1000
avg_time_taken_kubernetes/1000
avg_time_taken_cd/1000
avg_time_taken_cr/1000

sum(na.omit(avg_cpuusage_swarm/0.32))/length(na.omit(avg_cpuusage_swarm))
sum(na.omit(avg_cpuusage_cd/0.32))/length(na.omit(avg_cpuusage_cd))
sum(na.omit(avg_cpuusage/0.32))/length(na.omit(avg_cpuusage))

sum(na.omit(avg_memusage_swarm))/length(na.omit(avg_memusage_swarm))
sum(na.omit(avg_memusage_cr))/length(na.omit(avg_memusage_cr))
sum(na.omit(avg_memusage_cd))/length(na.omit(avg_memusage_cd))
sum(na.omit(avg_memusage))/length(na.omit(avg_memusage))

sum(na.omit(avg_networkusage_swarm))/length(na.omit(avg_networkusage_swarm))
sum(na.omit(avg_networkusage_cd))/length(na.omit(avg_networkusage_cd))
sum(na.omit(avg_networkusage))/length(na.omit(avg_networkusage))

sum(errorrate_swarm)/(100*1000)
sum(errorrate_kubernetes)/(100*1000)
sum(errorrate_kubernetes_cd)/(100*1000)
sum(errorrate_kubernetes_cr)/(100*1000)

sd_swarm
sd_kub
sd_kub_cd
sd_kub_cr