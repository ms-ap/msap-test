#!/bin/sh

testplan_normal="wordpress/wordpress.jmx"
testplan_load="wordpress/wordpress_load.jmx"
tags="wordpress"
service_protocol="https://"
service_url="192.168.0.2"
service_port="31443"
management_tool="kubernetes"
count=50
timeout=1800
retries=50
delay=15

if [ "$management_tool" = "kubernetes" ]
then
    ansible-playbook kubernetes/site.yml -t init,storage
elif [ "$management_tool" = "swarm" ]
then
    ansible-playbook swarm/site.yml -t init,storage
else
    echo "Unknown management_tool $management_tool"
    exit 1
fi

mvn package -f msapjmetertest/pom.xml && cp msapjmetertest/target/core-1.0-SNAPSHOT-jar-with-dependencies.jar .

java -jar core-1.0-SNAPSHOT-jar-with-dependencies.jar \
    $testplan_normal \
    --tags $tags \
    --service-protocol $service_protocol \
    --service-url $service_url \
    --service-port $service_port \
    --management-tool $management_tool \
    --replicas 1 \
    --timeout $timeout \
    --retries $retries \
    --count $count \
    --delay $delay

java -jar core-1.0-SNAPSHOT-jar-with-dependencies.jar \
    $testplan_normal \
    --tags $tags \
    --service-protocol $service_protocol \
    --service-url $service_url \
    --service-port $service_port \
    --management-tool $management_tool \
    --replicas 3 \
    --timeout $timeout \
    --retries $retries \
    --count $count \
    --delay $delay

java -jar core-1.0-SNAPSHOT-jar-with-dependencies.jar \
    $testplan_load \
    --tags $tags \
    --service-protocol $service_protocol \
    --service-url $service_url \
    --service-port $service_port \
    --management-tool $management_tool \
    --replicas 1 \
    --timeout $timeout \
    --retries $retries \
    --count $count
    --delay $delay

java -jar core-1.0-SNAPSHOT-jar-with-dependencies.jar \
    $testplan_load \
    --tags $tags \
    --service-protocol $service_protocol \
    --service-url $service_url \
    --service-port $service_port \
    --management-tool $management_tool \
    --replicas 3 \
    --timeout $timeout \
    --retries $retries \
    --count $count \
    --delay $delay
