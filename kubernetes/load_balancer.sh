# Create a test deployment
kubectl create web --image=gcr.io/google-samples/hello-app:1.0 --port=8080 --replicas 6

# Create a service with ClusterIP
kubectl expose deployment web --target-port=8080

# Create nginx ingress controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml

# Use the ingress controller to create a test service - https://kubernetes.io/docs/concepts/services-networking/ingress/#single-service-ingress
kubectl apply -f single_service_ingress.yml