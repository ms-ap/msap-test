#!/bin/sh

management_tool="kubernetes"
app_name="deployment/nginx-deployment"
app_namespace="test"
ctr=1
delay=30
replicas=1
logfile="$(date +%Y-%m-%d_%H-%M)_$management_tool.log"
prepared=1
verbose=0

start_test () {
    while [ "$ctr" -gt 0 ]; do
        sleep "$delay"
        if [ "$management_tool" = "kubernetes" ]; then
            run_kubernetes
        elif [ "$management_tool" = "swarm" ]
        then
            run_swarm
        else
            echo "Unknown management_tool $management_tool"
            exit 1
        fi

        ctr=$((ctr - 1))
    done
}

show_help () {
  echo "Usage: $0 [OPTION]"
  echo "Mandatory arguments
  -h for HELP
  -m MANAGEMENT_TOOL (swarm | kubernetes)
  -n APP_NAME (deployment/test-deployment)
  -s NAMESPACE (test)
  -c COUNTER (number of test runs)
  -d DELAY (delay between runs)
  -r REPLICAS (number of replicas)
  -f NAME OF LOGFILE
  -v VERBOSE"
}

prepare_kubernetes () {
    echo "Preparing test for kubernetes"
    prepared=0
    kubectl scale --replicas=2 "$app_name" -n "$app_namespace"
    kubectl rollout status "$app_name" -n "$app_namespace"
    kubectl scale --replicas=1 "$app_name" -n "$app_namespace"
    sleep "$delay"
}

run_kubernetes () {
    if [ "$prepared" -ne 0 ]; then prepare_kubernetes; fi
    kubectl scale --replicas="$replicas" "$app_name" -n "$app_namespace"
    /usr/bin/time -f "%e" -a -o "$logfile" kubectl rollout status "$app_name" -n "$app_namespace"
    kubectl scale --replicas=1 "$app_name" -n "$app_namespace"
}

prepare_swarm () {
    echo "Preparing test for docker swarm"
    prepared=0
    sudo docker service scale "$app_namespace"_"$app_name"=3
    sudo docker service scale "$app_namespace"_"$app_name"=1
    sleep "$delay"
}

run_swarm () {
     if [ "$prepared" -ne 0 ]; then prepare_swarm; fi
     /usr/bin/time -f "%e" -a -o "$logfile" sudo docker service scale "$app_namespace"_"$app_name"="$replicas"
     sudo docker service scale "$app_namespace"_"$app_name"=1
}

while getopts "h?vm:n:s:c:d:r:f:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  verbose=1
        set -xeu
        echo "verbose=$verbose, management_tool=$management_tool, app_name=$app_name, app_namespace=$app_namespace, ctr=$ctr, delay=$delay replicas=$replicas, logfile='$logfile' leftovers: $@"
        ;;
    m)  management_tool=$OPTARG
        ;;
    n)  app_name=$OPTARG
        ;;
    s)  app_namespace=$OPTARG
        ;;
    c)  ctr=$OPTARG
        ;;
    d)  delay=$OPTARG
        ;;
    r)  replicas=$OPTARG
        ;;
    f)  logfile=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

logfile="$(date +%Y-%m-%d_%H-%M)_$management_tool.log"
start_test