#!/usr/bin/env python3
"""
Use the undocumented Bitwarden API to generate keys
"""

# pylint: disable=import-error, invalid-name, unused-argument, unused-variable

import sys
import hashlib
import base64
import os
import json
import argparse
import cryptography
from cryptography.hazmat.primitives.ciphers import algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding

class Crypto():
    """Handles the crypto for Bitwarden"""
    def __init__(self, email, password, iterations):
        self.email = email.lower()
        self.password = password
        self.iterations = iterations
        # self.master_key = self.make_master_key()
        # self.master_password_hash = self.make_master_password_hash()
        # self.symmetric_key = os.urandom(64)
        # self.encryption_key = self.symmetric_key[:32]
        # self.mac_key = self.symmetric_key[32:64]
        # self.iv = os.urandom(16)
        # self.protected_symmetric_key = self.make_protected_symmetric_key()


    def make_master_key(self):
        """
        Generate master key
        :return: master key in byte format
        """
        return hashlib.pbkdf2_hmac(
            "sha256",
            self.password.encode(),
            self.email.encode(),
            self.iterations,
            dklen=32)

    def make_master_password_hash(self):
        """
        Generate hash of master key
        :return: base64 encoded hash of master key
        """
        return base64.b64encode(
            hashlib.pbkdf2_hmac(
                "sha256",
                self.make_master_key(),
                self.password.encode(),
                1,
                dklen=32))

    @staticmethod
    def encode_cipher_string(enctype, iv, ct, mac):
        """
        Encode cipher string (Bitwarden format)
        # pylint: disable=line-too-long
        :param enctype: encryption type https://github.com/bitwarden/browser/blob/f1262147a33f302b5e569f13f56739f05bbec362/src/services/constantsService.js#L13-L21
        :param iv: Initialization Vector
        :param ct: CipherText
        :param mac: Massage Authentication Code
        :return: Encoded cipher string
        """
        ret = f"{enctype}.{iv.decode()}|{ct.decode()}"
        if mac:
            return ret + '|' + mac.decode()
        return ret

    def make_protected_symmetric_key(self):
        """
        Generate protected symmetric key
        :return: Encoded cipher string
        """
        master_key = self.make_master_key()
        symmetric_key = os.urandom(64)
        iv = os.urandom(16)

        cipher = cryptography.hazmat.primitives.ciphers.Cipher(
            algorithms.AES(master_key), modes.CBC(iv), backend=default_backend())
        padder = padding.PKCS7(128).padder()
        padded_data = padder.update(symmetric_key)
        padded_data += padder.finalize()
        encryptor = cipher.encryptor()
        ct = encryptor.update(padded_data) + encryptor.finalize()
        return self.encode_cipher_string(0, base64.b64encode(iv), base64.b64encode(ct), None)


def print_register_body(args):
    """
    Get the registration body for Bitwarden API
    :param args: command line args
    :return: registration body
    """
    if args.email is None:
        my_crypto = Crypto(f"{args.username}@localhost.local", args.password, args.iterations)
    else:
        my_crypto = Crypto(args.email, args.password, args.iterations)
    sys.stdout.write(json.dumps(
        {
            "name": args.username,
            "email": my_crypto.email,
            "masterPasswordHash": my_crypto.make_master_password_hash().decode(),
            "masterPasswordHint": None,
            "key": my_crypto.make_protected_symmetric_key(),
            "kdf": 0,
            "kdfIterations": args.iterations
        }, indent=4))


def main():
    """
    main
    """
    # Add Parsers
    parser = argparse.ArgumentParser(description='Bitwarden API')

    subparsers = parser.add_subparsers(help='sub-command help')

    # create the parser for the register command
    parser_register = subparsers.add_parser('register', help='register user')
    parser_register.add_argument('-u', '--username',
                                 action='store',
                                 help='Name of the user')

    parser_register.add_argument('-e', '--email',
                                 action='store',
                                 help='Email of the user')

    parser_register.add_argument('-p', '--password',
                                 action='store',
                                 help='Password of user')

    parser_register.add_argument('-i', '--pbkdf2-iterations',
                                 action='store',
                                 dest='iterations',
                                 type=int,
                                 default=100000,
                                 help='Number of pbkdf2 iterations')

    parser_register.set_defaults(func=print_register_body)

    # Parse command line arguments
    args = parser.parse_args()

    # Call functions based on user input
    try:
        args.func(args)
    except AttributeError as e:
        parser.print_help()

if __name__ == '__main__':
    main()
