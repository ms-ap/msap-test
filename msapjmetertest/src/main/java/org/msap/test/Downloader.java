package org.msap.test;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

class Downloader {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRunner.class);

    private Downloader() {}

    static File downloadFile(String url, String dest) throws IOException {
        File f = new File(dest);
        URL u = new URL(url);
        if (!f.exists()) {
            LOGGER.info("Downloading file from {}", url);
            FileUtils.copyURLToFile(u, f, 30000, 30000);
        } else {
            LOGGER.info("File {} already exists. Skipping download", f.getAbsolutePath());
        }

        return f;
    }
}
