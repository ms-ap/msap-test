package org.msap.test;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Deployment
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Deployment.class);
    private String playbookPath;
    private String tags;
    private boolean vagrant;
    private String readinessUrl;
    private String extraVars;

    Deployment(String managementTool, String tags, int replicas, boolean vagrant, String readinessUrl) {
        this.tags = tags;
        this.vagrant = vagrant;
        this.readinessUrl = readinessUrl;

        this.extraVars = "replicas=" + replicas;

        if(vagrant) {
            this.playbookPath = "/vagrant/msap-test/" + managementTool;
        }
        else {
            this.playbookPath = managementTool;
        }
    }

    boolean create() throws IOException {
        String[] ansibleCMD;

        if (vagrant) {
            ansibleCMD = new String[]{"vagrant", "ssh", "master", "-c ansible-playbook " + playbookPath + "/site.yml -e \"" + extraVars + "\" -t " + tags};

        }
        else {
            File playbook = new File(playbookPath + "/site.yml");
            if (!playbook.exists()) throw new FileNotFoundException("File " + playbook + " not found!");
            ansibleCMD = new String[]{"ansible-playbook", playbook.getAbsolutePath(), "-e", extraVars, "-t", tags};
        }

        LOGGER.info("Creating deployment");
        if (runProcess(ansibleCMD) != 0) {
            LOGGER.error("Could not create deployment");
            return false;
        }

        return true;
    }

    boolean delete() throws IOException {
        String[] ansibleCMD;

        if (vagrant) {
            ansibleCMD = new String[]{"vagrant", "ssh", "master", "-c ansible-playbook " + playbookPath + "/reset_site.yml -t " + tags};

        }
        else {
            File playbook = new File(playbookPath + "/reset_site.yml");
            if (!playbook.exists()) throw new FileNotFoundException("File " + playbook + " not found!");
            ansibleCMD = new String[]{"ansible-playbook", playbook.getAbsolutePath(), "-t", tags};
        }

        LOGGER.info("Deleting deployment");
        if (runProcess(ansibleCMD) != 0) {
            LOGGER.error("Could not delete deployment");
            return false;
        }

        return true;
    }

    boolean isReady() throws IOException {
        URL url = new URL(readinessUrl);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("GET");

        LOGGER.info("Waiting for {} to return HTTP code {}", url, HttpStatus.SC_OK);
        try {
            conn.connect();
            int responseCode = conn.getResponseCode();
            LOGGER.info("Got response code {}", responseCode);
            if (responseCode == HttpStatus.SC_OK || responseCode==302) return true;
        }
        catch (ConnectException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
        return false;
    }

    private int runProcess(String[] command) throws IOException {
        LOGGER.debug("Running command '{}'", (Object) command);
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.inheritIO().redirectOutput(ProcessBuilder.Redirect.PIPE);
        Process process = builder.start();

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
            reader.lines().forEach(LOGGER::info);
        }

        try {
            int returnCode = process.waitFor();
            LOGGER.debug("Process exited with return code: {}", returnCode);
            return returnCode;
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
            return 1;
        }
    }
}
