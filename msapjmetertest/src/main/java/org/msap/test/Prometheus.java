package org.msap.test;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class Prometheus {
    private static final Logger LOGGER = LoggerFactory.getLogger(Prometheus.class);
    private Prometheus(){

    }
    private static int retries=0;

    static String[] getMetricsFromProm(String promUrl, String[] metrics, String metricsPath) throws IOException {
        LOGGER.info("Gathering Metrics from Prometheus");
        String[] result = new String[metrics.length];
        for (int i = 0; i < metrics.length; i++) {
            String metricspath = promUrl + metricsPath + metrics[i];
            URL url = new URL(metricspath);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if(con.getResponseCode()==422 || con.getResponseCode()==HttpURLConnection.HTTP_BAD_REQUEST) {
                try {
                    LOGGER.warn("Got invalid response. Retrying");
                    if(retries>=5&&metrics[i].contains("mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='1'}")){
                        metrics[i]="rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='1',pod=~\"my.*\"}[5m])-on(instance)rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='0.1',pod=~\"my.*\"}[5m])";
                    } else if (retries>=6){
                        LOGGER.warn("Mysql-Exporter broken again resetting loop");
                        retries=0;
                        return null;
                    }
                    Thread.sleep(5000);
                    retries++;
                    i--;
                    continue;
                } catch (InterruptedException e){
                    LOGGER.warn(e.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            result[i] = content.toString();
        }
        return result;
    }

    static String[][] splitResults(String[] results){
        LOGGER.info("Splitting Query Results");
        if (results==null){
            return null;
        }
        String[][] output = null;
        for (int i = 0; i < results.length; i++) {
            if(results[i].contains("[]")){
                continue;
            }
            String[] splitvalues = results[i].split("values\":\\[\\[")[1].split("\\],\\[");
            splitvalues[splitvalues.length - 1] = splitvalues[splitvalues.length - 1].split("\\]")[0];
            if (output == null) {
                output = new String[results.length +1][splitvalues.length];
            }
            if (i == 0) {
                for (int x = 0; x < splitvalues.length; x++) {
                    output[i][x] = splitvalues[x].split(",")[0];
                    output[i + 1][x] = splitvalues[x].split(",")[1];
                }
            } else {
                for (int x = 0; x < splitvalues.length; x++) {
                    output[i + 1][x] = splitvalues[x].split(",")[1];
                }
            }
        }
        return output;
    }

    public static String[][] readJmeterDataFromCSVFile(String filename) throws IOException {
        LOGGER.info("Reading Jmeter Data from CSV File");
        String[] headers = { "timeStamp","elapsed","label","responseCode","responseMessage","threadName","dataType","success","failureMessage","bytes","sentBytes","grpThreads","allThreads","URL","Latency","IdleTime","Connect"};
        Reader in = new FileReader(filename);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withHeader(headers)
                .withFirstRecordAsHeader()
                .parse(in);
        String[][]output = new String[4][((CSVParser) records).getRecords().size()];
        in.close();
        in = new FileReader(filename);
        records = CSVFormat.DEFAULT
                .withHeader(headers)
                .withFirstRecordAsHeader()
                .parse(in);
        int i = 0;
        for (CSVRecord record : records){
            output[0][i]=record.get(headers[1]);
            output[1][i]=record.get(headers[2]);
            output[2][i]=record.get(headers[3]);
            output[3][i]=record.get(headers[14]);
            i++;
        }
        return output;
    }


    static String[] readEpochFromCSVFile(String filename) throws IOException {
        LOGGER.info("Reading Epoch from CSV File");
        String[] result = new String[2];
        String[] headers = { "timeStamp","elapsed","label","responseCode","responseMessage","threadName","dataType","success","failureMessage","bytes","sentBytes","grpThreads","allThreads","URL","Latency","IdleTime","Connect"};
        Reader in = new FileReader(filename);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withHeader(headers)
                .withFirstRecordAsHeader()
                .parse(in);
        for (CSVRecord record : records){
            if(result[0]==null&& !record.get(headers[2]).contains("setUp")){
                result[0]=record.get(headers[0]).substring(0,10);
            }
                result[1]= record.get(headers[0]).substring(0,10);

        }
        in.close();
        return result;
    }

    static boolean verifyWrittenResults(String filename) throws IOException{
        LOGGER.info("verifying Results written to CSV File");
        Reader in = new FileReader(filename);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .parse(in);
        for (CSVRecord record : records){
            if((record.get(record.size()-1).contains("null"))||(record.get(record.size()-2).contains("null"))||(record.get(record.size()-3).contains("null"))){
                LOGGER.warn("Metrics broken -- Retrying");
                return false;
            }
        }
        in.close();
        return true;
    }

    static boolean writeResultsToCSVFile(String[][] output,String[]metrics ,String filename) throws IOException {
        LOGGER.info("Writing Results to CSV File");
        if(output==null){
            return false;
        }
        try (FileWriter out = new FileWriter(filename)) {
            CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT);
            printer.print("epoch");
            for (String metric : metrics) {
                printer.print(metric);
            }
            printer.println();
            for (int x = 0; x < output[0].length; x++) {
                for (int i = 0; i < output.length; i++) {
                    if(output[i][x]==null){
                        printer.print("null");
                        continue;
                    }

                    output[i][x] = output[i][x].replaceAll("\"", "");
                    printer.print(output[i][x]);
                }
                printer.println();
            }


        } catch (NullPointerException e) {
            return false;
        }
        return verifyWrittenResults(filename);
    }
}

