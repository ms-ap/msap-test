package org.msap.test;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

class Unzipper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRunner.class);

    private  Unzipper() {}

    static void unzipFile(String filename, String destDir) throws IOException {
        File targetDir = new File(destDir);

        try (
                InputStream fi = Files.newInputStream(Paths.get(filename));
                ArchiveInputStream i = new ZipArchiveInputStream(fi)
        ) {
            ArchiveEntry entry;
            while ((entry = i.getNextEntry()) != null) {
                if (!i.canReadEntryData(entry)) {
                    LOGGER.warn("Cannot read {}", entry.getName());
                    continue;
                }
                String name = fileName(targetDir, entry);
                File f = new File(name);
                if (f.exists()) return;
                LOGGER.debug("Extracting {} to {}", f.getAbsolutePath(), targetDir.getAbsolutePath());
                if (entry.isDirectory()) {
                    if (!f.isDirectory() && !f.mkdirs()) {
                        throw new IOException("failed to create directory " + f);
                    }
                } else {
                    File parent = f.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("failed to create directory " + parent);
                    }
                    try (OutputStream o = Files.newOutputStream(f.toPath())) {
                        IOUtils.copy(i, o);
                    }
                }
            }
        }
    }

    private static String fileName(File targetDir, ArchiveEntry entry) {
        if (targetDir.mkdirs()) LOGGER.info("Created target directory {}", targetDir);
        return targetDir.getAbsolutePath() + File.separator + entry.getName();
    }
}
