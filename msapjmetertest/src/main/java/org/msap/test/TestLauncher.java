package org.msap.test;

import java.io.*;
import java.nio.file.Files;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;


public class TestLauncher {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestLauncher.class);
    private static final String[] BAD_LIBS = {"mongo-java-driver-2.11.3.jar", "groovy-all-2.4.16.jar"};
    private static String jmeterVersion;
    private static String jmeterHome;

    private static Properties getProperties() throws IOException {
        Properties p = new Properties();
        InputStream in = TestLauncher.class.getResourceAsStream("/msap.properties");
        if (in == null) {
            throw new NullPointerException("Error while loading properties. You should run maven compile first");
        }
        p.load(in);

        return p;
    }

    private static void setConstantsFromProperties() throws IOException {
        Properties p = getProperties();
        jmeterVersion = p.getProperty("jmeter.version");
        jmeterHome = System.getProperty("user.dir") + "/jmeter/apache-jmeter-" + jmeterVersion;
    }

    private static void preRunChecks() throws IOException {
        // Set constants
        setConstantsFromProperties();

        //Download JMeter
        File jmeterZip = Downloader.downloadFile(
                "https://www-us.apache.org/dist/jmeter/binaries/apache-jmeter-" + jmeterVersion + ".zip",
                "jmeter/jmeter" + jmeterVersion +".zip"
        );

        Unzipper.unzipFile(jmeterZip.getAbsolutePath(), "jmeter/");

        LOGGER.info("Renaming some JMeter lib files that cause trouble");
        File[] libFiles = new File(jmeterHome, "lib").listFiles();
        if (libFiles != null) {
            for (File file : libFiles) {
                for (String badLib : BAD_LIBS) {
                    if (file.getName().matches(badLib)) {
                        LOGGER.info("Renaming conflicting JMeter lib:\n\t{} -> {}", file.getAbsoluteFile(), file.getAbsoluteFile() + ".bak");
                        Files.move(file.toPath(), new File(file.getAbsoluteFile() + ".bak").toPath());
                    }
                }
            }
        }
        // Modify user.properties
        try (OutputStream output = new FileOutputStream(jmeterHome + "/bin/user.properties"))  {
            Properties userProperties = new Properties();
            userProperties.setProperty("plugin_dependency_paths", new File(System.getProperty("user.dir") + "/msapjmetertest/target/dependency").getAbsolutePath());
            userProperties.store(output, null);
        }


    }

    public static void main(String[] args) throws IOException {
        LOGGER.info("Running Pre-Run checks");
        preRunChecks();

        try {
            TestRunner testRunner = CommandLine.populateCommand(new TestRunner(
                    jmeterHome,
                    jmeterHome + "/bin/jmeter.properties",
                    "/jmeter-testplans"
            ), args);

            testRunner.start();
        } catch (CommandLine.ParameterException e) {
            LOGGER.error("{}\n{}", e, e.getCommandLine().getUsageMessage());
        }
    }
}
