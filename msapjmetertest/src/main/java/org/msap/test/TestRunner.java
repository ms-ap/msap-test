package org.msap.test;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Task;
import com.github.dockerjava.core.DockerClientBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;
import org.apache.jorphan.collections.ListedHashTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Option;
import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static picocli.CommandLine.*;

class TestRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRunner.class);
    private static final String PREFIX = "msap_";
    private static final String SUFFIX = ".tmp";
    private static final String SWARM = "swarm";

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
    private final File logFileDir = new File("logs/" + simpleDateFormat.format(new Date()));

    private File jmeterHome;
    private File jmeterProperties;
    private String jmeterTestplans;
    private String[] metrics;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "Display this help")
    private boolean helpRequested = false;

    @Parameters(index = "0")
    private String jmeterTestplan;

    @Option(names= { "-g", "--vagrant" }, description = "Call ansible inside Vagrant")
    private boolean vagrant = false;

    @Option(names= { "-m", "--management-tool" }, required = true, description = "Choose the managament tool which should be used\n\t" +
            "Accepted values: [kubernetes, swarm]")
    private String managementTool;

    @Option(names= { "-r", "--replicas" }, description = "Chose the number of replicas which should be deployed")
    private int deploymentReplicas = 1;

    @Option(names = { "-t", "--tags" }, description = "Specify a list of comma separated tags that will be passed to ansible")
    private String tags;

    @Option(names = { "--skip-deployment" }, description = "Skip the deployment of the app (only run the test)")
    private boolean skipDeployment = false;

    @Option(names = { "--skip-readiness-check" }, description = "Skip the readiness check (run test immediately)")
    private boolean skipReadinessCheck = false;

    @Option(names = { "--timeout" }, description = "Maximum time (in seconds) to wait for the deployment to become ready")
    private int timeout = 600;

    @Option(names = { "--service-protocol" }, description = "Set the protocol (http:// https://) on which the service is listening")
    private String serviceProtocol = "http://";

    @Option(names = { "--service-url" }, description = "Set the IP on which the service is listening")
    private String serviceUrl = "192.168.90.2";

    @Option(names = { "--service-port" }, description = "Set the port on which the service is listening")
    private String servicePort = "31080";

    @Option(names = { "-c", "--count" }, description = "Set the number of test runs (deploy, run test, delete)")
    private int loopCount = 1;
    private int currentLoop;

    @Option(names = { "-d", "--delay" }, description = "Set the delay between test re-runs (in seconds)")
    private int loopDelay = 10;

    @Option(names = { "--retries" }, description = "Set the number of retries for the creation of the deployment")
    private int retries = 5;

    TestRunner(String jmeterHome, String jmeterProperties, String jmeterTestplans) throws IOException {
        this.jmeterHome = new File(jmeterHome);
        this.jmeterProperties = new File(jmeterProperties);
        this.jmeterTestplans = jmeterTestplans;

        LOGGER.debug("Created new TestRunner with \n\t" +
                "jmeterHome: {}\n\t" +
                "jmeterProperties: {}\n\t" +
                "jmeterTestPlans: {}",
                this.jmeterHome.getAbsolutePath(),
                this.jmeterProperties.getAbsolutePath(),
                this.jmeterTestplans);
        initJmeter();
    }

    void start() throws IOException {
        if (skipDeployment) LOGGER.info("Skipping deployment (only run tests)");
        if (skipReadinessCheck) LOGGER.info("Skipping readinessCheck (running test immediately)");

        // Choose the test which should be started
        for (currentLoop = 1; currentLoop <= loopCount; currentLoop++) {
            LOGGER.info("Running test loop {}/{}", currentLoop, loopCount);
            if (jmeterTestplan.contains("gitlab")) {//NOSONAR
                runGitlabTest();
            } else if (jmeterTestplan.contains("rocketchat")) {
                runRocketchatTest();
            } else if (jmeterTestplan.contains("wordpress")) { //NOSONAR
                runWordpressTest();
            } else if (tags.contains("job")) {
                runJobTest();
            } else {
                runTest(getClass().getResourceAsStream(jmeterTestplans + "/" + jmeterTestplan));
            }
            if (currentLoop != loopCount) try {
                LOGGER.info("Waiting {} seconds between test runs", loopDelay);
                Thread.sleep(loopDelay * 1_000L);
            } catch (InterruptedException e) {
                LOGGER.warn(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    private void validateArgs() {
        if (!managementTool.matches("swarm|kubernetes")) {
            LOGGER.error("Unknown option {}. Exiting.", managementTool);
            System.exit(1);
        }
    }

    private void initJmeter () throws IOException {
        // Initialize Properties, logging, locale, etc.
        LOGGER.debug("Initializing JMeter");
        JMeterUtils.loadJMeterProperties(jmeterProperties.getAbsolutePath());
        JMeterUtils.setJMeterHome(jmeterHome.getAbsolutePath());
        JMeterUtils.initLocale();

        // Initialize JMeter SaveService
        SaveService.loadProperties();
    }

    private File streamToTempFile(InputStream in, String prefix, String suffix) throws IOException {
        File tempFile = File.createTempFile(prefix, suffix);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }

    private void deployApp(Deployment deployment) throws IOException {
        // Deploy the application in the cluster
        // Try to create the deployment 5 times
        for (int i = retries; i >= 0; i--) {
            if (deployment.create()) return;
            LOGGER.warn("Retrying to create deployment {} more times", i);
            deployment.delete();
        }

        // End test if creation failed 5 times
        LOGGER.error("Ending tests");
        System.exit(1);
    }

    private void deleteApp(Deployment deployment) throws IOException {
        // Delete the application from the cluster
        // Try to delete the deployment 5 times
        for (int i = retries; i >= 0; i--) {
            if (deployment.delete()) return;
            LOGGER.warn("Retrying to create deployment {} more times", i);
        }

        // End test if deletion failed 5 times
        LOGGER.error("Ending tests");
        System.exit(1);
    }

    private boolean appIsReady(Deployment deployment) throws IOException {
        int timeBetweenChecks = 5000;
        int remainingWaitTime = timeout * 1000;

        do {
            try {
                Thread.sleep(timeBetweenChecks);
                remainingWaitTime -= timeBetweenChecks;
            } catch (InterruptedException e) {
                LOGGER.warn(e.getMessage());
                Thread.currentThread().interrupt();
            }
            LOGGER.info("Trying for another {} seconds", remainingWaitTime / 1000);
        } while (!deployment.isReady() && remainingWaitTime > 0);

        // If the timeout has passed delete the deployment
        if (remainingWaitTime <= 0) {
            LOGGER.warn("Timeout ({}s) passed", timeout);
            currentLoop--;
            LOGGER.info("Resetting currentLoop to {}", currentLoop);
            deployment.delete();
            return false;
        }
        try {
            LOGGER.info("Waiting additional 120s after deployment for Prometheus metrics");
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            LOGGER.warn(e.getMessage());
            Thread.currentThread().interrupt();
        }
        return true;
    }

    private void runTest(InputStream testPlanStream) throws IOException {
        HashTree testPlanTree = SaveService.loadTree(streamToTempFile(testPlanStream, PREFIX, SUFFIX));
        runTest(testPlanTree);
    }

    private void runTest(HashTree testPlanTree) throws IOException {
        // Validate commandline arguments
        validateArgs();

        // Set metrics for Prometheus query depending on Managementtool and scenario
        metrics = setMetrics();
        // JMeter Engine
        StandardJMeterEngine jmeter = new StandardJMeterEngine();

        // Jmeter Logging
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }

        // Get the filename for the logger from filename of the testplan without extension
        String[] filenameSplit = jmeterTestplan.split("[/|\\\\]");
        String filename = filenameSplit[filenameSplit.length - 1].split("\\.")[0] + "_" + deploymentReplicas + "rep_" + managementTool + "_" + currentLoop + ".csv";

        // Delete file if it's already there to avoid appending
        File logFile = new File(logFileDir, filename);
        if (logFile.exists()) Files.delete(logFile.toPath());

        // Add logger
        ResultCollector logger = new ResultCollector(summer);
        logger.setFilename(logFile.getAbsolutePath());
        testPlanTree.add(testPlanTree.getArray()[0], logger);

        // Deploy the application in the cluster
        Deployment deployment = new Deployment(managementTool, tags, deploymentReplicas, vagrant, serviceProtocol + serviceUrl + ":" + servicePort);
        if (!skipDeployment) deployApp(deployment);

        // Wait for deployment to become ready
        if (!skipReadinessCheck && !appIsReady(deployment)) {
            // Re-run if deployment was not ready within time
            return;
        }

        // Run JMeter Test
        jmeter.configure(testPlanTree);
        LOGGER.info("Running tests. This may take a while...");
        jmeter.run();
        String[]metricqueries = new String[metrics.length];
        String[]epochs = Prometheus.readEpochFromCSVFile(logger.getFilename());
        for (int i=0;i<metrics.length;i++){
            metricqueries[i]=metrics[i]+"&start="+epochs[0]+"&end="+epochs[1]+"&step=3";
        }
        boolean erg= Prometheus.writeResultsToCSVFile(Prometheus.splitResults(Prometheus.getMetricsFromProm("http://" + serviceUrl, metricqueries, ":30090/api/v1/query_range?query=")),metrics, logFileDir + "/prom_" + filename);

        // Destroy the deployment
        if (!skipDeployment) deleteApp(deployment);

        if(!erg){
            currentLoop--;
            LOGGER.info("Resetting currentLoop to {}", currentLoop);
        }
    }
    @SuppressWarnings({"squid:S3776","squid:S1192"})
    private String[] setMetrics(){
        if(managementTool.equals(SWARM)){
            if (tags.contains("galera")){
                metrics = new String[]{"sum(container_memory_usage_bytes{container_label_com_docker_stack_namespace=\"galera\"})", "sum(container_network_receive_packets_dropped_total{container_label_com_docker_stack_namespace=\"galera\"})", "sum(container_network_transmit_packets_dropped_total{container_label_com_docker_stack_namespace=\"galera\"})","sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_stack_namespace=\"galera\"}[5m]))","sum(irate(container_network_receive_bytes_total{container_label_com_docker_stack_namespace=\"galera\"}[5m]))","sum(mysql_global_status_connection_errors_total)","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysql-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysql-exporter\"}[5m])*1000","rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysql-exporter\",le='1'}[5m])-on(instance)rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysql-exporter\",le='0.1'}[5m])","rate(mysql_info_schema_read_query_response_time_seconds_sum{job=\"mysql-exporter\"}[5m])/rate(mysql_info_schema_read_query_response_time_seconds_count{job=\"mysql-exporter\"}[5m])*1000","rate(mysql_info_schema_write_query_response_time_seconds_sum{job=\"mysql-exporter\"}[5m])/rate(mysql_info_schema_write_query_response_time_seconds_count{job=\"mysql-exporter\"}[5m])*1000"};
            } else if (tags.contains("website")){
                metrics = new String[]{"sum(container_memory_usage_bytes{container_label_com_docker_stack_namespace=\"website\"})", "sum(container_network_receive_packets_dropped_total{container_label_com_docker_stack_namespace=\"website\"})", "sum(container_network_transmit_packets_dropped_total{container_label_com_docker_stack_namespace=\"website\"})","sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_stack_namespace=\"website\"}[5m]))","sum(irate(container_network_receive_bytes_total{container_label_com_docker_stack_namespace=\"website\"}[5m]))"};
            } else if (tags.contains("job")){
                metrics = new String[]{"sum(node_memory_MemTotal_bytes)-sum(node_memory_MemFree_bytes)","sum(100-(avg%20by(instance)(irate(node_cpu_seconds_total{job=\"nodeexporter\",mode=\"idle\"}[5m]))*100))/4"};
            } else if (tags.contains("chat")){
                metrics = new String[]{"sum(container_memory_usage_bytes{container_label_com_docker_stack_namespace=\"chat\"})", "sum(container_network_receive_packets_dropped_total{container_label_com_docker_stack_namespace=\"chat\"})", "sum(container_network_transmit_packets_dropped_total{container_label_com_docker_stack_namespace=\"chat\"})","sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_stack_namespace=\"chat\"}[5m]))","sum(irate(container_network_receive_bytes_total{container_label_com_docker_stack_namespace=\"chat\"}[5m]))","mongodb_extra_info_page_faults_total","mongodb_network_bytes_total{state=\"out_bytes\"}","mongodb_network_bytes_total{state=\"in_bytes\"}","mongodb_network_metrics_num_requests_total"};
            } else if (tags.contains("gitlab")){
                metrics = new String[]{"sum(node_memory_MemTotal_bytes)-sum(node_memory_MemFree_bytes)","sum(rate(node_cpu_seconds_total[5m]))","sum(container_memory_usage_bytes{container_label_com_docker_stack_namespace=\"git\"})", "sum(container_network_receive_packets_dropped_total{container_label_com_docker_stack_namespace=\"git\"})", "sum(container_network_transmit_packets_dropped_total{container_label_com_docker_stack_namespace=\"git\"})","sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_stack_namespace=\"git\"}[5m]))","sum(irate(container_network_receive_bytes_total{container_label_com_docker_stack_namespace=\"git\"}[5m]))"};
            } else if (tags.contains("wordpress")){
                metrics = new String[]{"sum(container_memory_usage_bytes{container_label_com_docker_stack_namespace=\"wp\"})", "sum(container_network_receive_packets_dropped_total{container_label_com_docker_stack_namespace=\"wp\"})", "sum(container_network_transmit_packets_dropped_total{container_label_com_docker_stack_namespace=\"wp\"})","sum(rate(container_cpu_usage_seconds_total{container_label_com_docker_stack_namespace=\"wp\"}[5m]))","sum(irate(container_network_receive_bytes_total{container_label_com_docker_stack_namespace=\"wp\"}[5m]))","sum(mysql_global_status_connection_errors_total)","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysql-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysql-exporter\"}[5m])*1000","rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysql-exporter\",le='1'}[5m])-on(instance)rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysql-exporter\",le='0.1'}[5m])","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysql-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysql-exporter\"}[5m])*1000"};
            }
        } else if (managementTool.equals("kubernetes")){
            if(tags.contains("wordpress-xtradb")){
                metrics = new String[]{"sum(container_memory_usage_bytes{namespace=\"wordpress-xtradb\"})", "sum(container_network_receive_packets_dropped_total{namespace=\"wordpress-xtradb\"})", "sum(container_network_transmit_packets_dropped_total{namespace=\"wordpress-xtradb\"})","sum(namespace_pod_name_container_name:container_cpu_usage_seconds_total:sum_rate{namespace=\"wordpress-xtradb\"})","sum(irate(container_network_receive_bytes_total{namespace=\"wordpress-xtradb\"}[5m]))","sum(mysql_global_status_connection_errors_total)","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysqld-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysqld-exporter\"}[5m])*1000","rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='1',pod=~\"my.*\"}[5m])-on(instance)rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='0.1',pod=~\"my.*\"}[5m])","rate(mysql_info_schema_read_query_response_time_seconds_sum{job=\"mysqld-exporter\"}[5m])/rate(mysql_info_schema_read_query_response_time_seconds_count{job=\"mysqld-exporter\"}[5m])*1000","rate(mysql_info_schema_write_query_response_time_seconds_sum{job=\"mysqld-exporter\"}[5m])/rate(mysql_info_schema_write_query_response_time_seconds_count{job=\"mysqld-exporter\"}[5m])*1000"}; //NOSONAR
            } else if(tags.contains("website")){
                metrics = new String[]{"sum(container_memory_usage_bytes{namespace=\"static-website\"})", "sum(container_network_receive_packets_dropped_total{namespace=\"static-website\"})", "sum(container_network_transmit_packets_dropped_total{namespace=\"static-website\"})","sum(namespace_pod_name_container_name:container_cpu_usage_seconds_total:sum_rate{namespace=\"static-website\"})","sum(irate(container_network_receive_bytes_total{namespace=\"static-website\"}[5m]))"};
            } else if (tags.contains("job")){
                metrics = new String[]{"sum(node_memory_MemTotal_bytes)-sum(node_memory_MemFree_bytes)","sum(100-(avg%20by(instance)(irate(node_cpu_seconds_total{job=\"node-exporter\",mode=\"idle\"}[5m]))*100))/4"};
            } else if (tags.contains("rocketchat")){
                metrics = new String[]{"sum(container_memory_usage_bytes{namespace=\"rocketchat\"})", "sum(container_network_receive_packets_dropped_total{namespace=\"rocketchat\"})", "sum(container_network_transmit_packets_dropped_total{namespace=\"rocketchat\"})","sum(namespace_pod_name_container_name:container_cpu_usage_seconds_total:sum_rate{namespace=\"rocketchat\"})","sum(irate(container_network_receive_bytes_total{namespace=\"rocketchat\"}[5m]))","mongodb_extra_info_page_faults_total{namespace=\"rocketchat\"}","mongodb_network_bytes_total{state=\"out_bytes\"}","mongodb_network_bytes_total{state=\"in_bytes\"}","mongodb_network_metrics_num_requests_total"};
            } else if (tags.contains("gitlab")){
                metrics = new String[]{ "sum(container_network_transmit_packets_dropped_total{namespace=\"gitlab\"})","sum(rate(node_cpu_seconds_total[5m]))","sum(irate(container_network_receive_bytes_total{namespace=\"gitlab\"}[5m]))", "sum(container_network_receive_packets_dropped_total{namespace=\"gitlab\"})","sum(node_memory_MemTotal_bytes)-sum(node_memory_MemFree_bytes)","sum(container_memory_usage_bytes{namespace=\"gitlab\"})","sum(namespace_pod_name_container_name:container_cpu_usage_seconds_total:sum_rate{namespace=\"gitlab\"})"};
            } else if (tags.contains("wordpress")){
                metrics = new String[]{"sum(container_memory_usage_bytes{namespace=\"wordpress\"})", "sum(container_network_receive_packets_dropped_total{namespace=\"wordpress\"})", "sum(container_network_transmit_packets_dropped_total{namespace=\"wordpress\"})","sum(namespace_pod_name_container_name:container_cpu_usage_seconds_total:sum_rate{namespace=\"wordpress\"})","sum(irate(container_network_receive_bytes_total{namespace=\"wordpress\"}[5m]))","sum(mysql_global_status_connection_errors_total)","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysqld-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysqld-exporter\"}[5m])*1000","rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='1',pod=~\"my.*\"}[5m])-on(instance)rate(mysql_info_schema_query_response_time_seconds_bucket{job=\"mysqld-exporter\",le='0.1',pod=~\"my.*\"}[5m])","rate(mysql_info_schema_query_response_time_seconds_sum{job=\"mysqld-exporter\"}[5m])/rate(mysql_info_schema_query_response_time_seconds_count{job=\"mysqld-exporter\"}[5m])*1000"}; //NOSONAR
            }
        }
        return metrics;
    }

    private void runGitlabTest() throws IOException {
        InputStream testPlanStream = getClass().getResourceAsStream(jmeterTestplans + "/" + jmeterTestplan);

        InputStream sshKeyFileStream = getClass().getResourceAsStream(jmeterTestplans + "/gitlab/ssh-keys.csv");
        InputStream dockerImageListStream = getClass().getResourceAsStream(jmeterTestplans + "/gitlab/dockerImageList.csv");

        // Get HashTree from testplan
        HashTree testPlanTree = SaveService.loadTree(streamToTempFile(testPlanStream, PREFIX, SUFFIX));

        // Get path of resources
        String sshKeyFilePath = streamToTempFile(sshKeyFileStream, PREFIX, SUFFIX).getAbsolutePath();
        String dockerImageListPath = streamToTempFile(dockerImageListStream, PREFIX, SUFFIX).getAbsolutePath();

        //Set value of the sshKeyFile variable to the right filepath
        replaceArgumentInHashTree(testPlanTree, "sshKeyFile", sshKeyFilePath);
        replaceArgumentInHashTree(testPlanTree, "dockerImageList", dockerImageListPath);

        runTest(testPlanTree);
    }

    private void runRocketchatTest() throws IOException {
        InputStream testPlanStream = getClass().getResourceAsStream(jmeterTestplans + "/" + jmeterTestplan);
        InputStream jpegFileStream = getClass().getResourceAsStream(jmeterTestplans + "/rocketchat/wal.jpeg");

        // Get HashTree from testplan
        HashTree testPlanTree = SaveService.loadTree(streamToTempFile(testPlanStream, PREFIX, SUFFIX));
        String imageFilePath = streamToTempFile(jpegFileStream, PREFIX, SUFFIX).getAbsolutePath();

        //Set value of the image file variable to the right filepath
        replaceArgumentInHashTree(testPlanTree, "uploadFilePath", imageFilePath);

        runTest(testPlanTree);
    }

    private void runJobTest() throws IOException {
        String[] metricstring=setMetrics();
        logFileDir.mkdir();
        String filename = "job" + "_" + deploymentReplicas + "rep_" + managementTool + "_" + currentLoop + ".csv";
        validateArgs();
        long start=0;
        String startstring=null;
        long end=0;
        String endstring=null;
        Deployment deployment = new Deployment(managementTool, tags, deploymentReplicas, vagrant, serviceProtocol + serviceUrl + ":" + servicePort);
        if (!skipDeployment) deployApp(deployment);

        if (managementTool.equals(SWARM)) {
            DockerClient officialClient = DockerClientBuilder.getInstance("tcp://"+serviceUrl+":2375").build();
            List<Task> job = officialClient.listTasksCmd().withServiceFilter("job_job").exec();
            boolean completed=false;
            SimpleDateFormat date =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            date.setTimeZone(TimeZone.getTimeZone("GMT"));
            while(!completed){
                for(int i=0; i<job.size();i++){
                    completed=job.get(i).getStatus().getState().getValue().equals("complete");
                    if(completed==false){
                        break;
                    }
                    try {
                        long tmp = date.parse(job.get(i).getCreatedAt().substring(0,22)).getTime();
                        if(start==0||tmp<start){
                            start=tmp;
                        }
                        tmp = date.parse(job.get(i).getUpdatedAt().substring(0,22)).getTime();
                        if(end<tmp){
                            end=tmp;
                        }
                    } catch (Exception e){
                        LOGGER.debug("Exception occured");
                    }
                }
                try {
                    LOGGER.info("Waiting 10s for job to finish");
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    LOGGER.warn(e.getMessage());
                    Thread.currentThread().interrupt();
                }
                job = officialClient.listTasksCmd().withServiceFilter("job_job").exec();
            }
            try(FileWriter fw = new FileWriter(logFileDir +"/"+  filename)){
                fw.write(Long.toString(end-start).substring(0,Long.toString(end-start).length()-3));
            }
            startstring =Long.toString(start).substring(0,10);
            endstring =Long.toString(end).substring(0,10);
        } else if (managementTool.equals("kubernetes")) {
            String[] result=null;
            String[] metricquery =new String[]{"kube_job_status_completion_time{namespace=\"job\"}-kube_job_created{namespace=\"job\"}"};
            result = new String[1];
            result[0] = Prometheus.getMetricsFromProm("http://" + serviceUrl, metricquery, ":30090/api/v1/query?query=")[0];
            while (result[0].contains("[]")) {
                try {
                    LOGGER.info("Waiting 10s for job to finish");
                    Thread.sleep(10000);
                    result[0] = Prometheus.getMetricsFromProm("http://" + serviceUrl, metricquery, ":30090/api/v1/query?query=")[0];

                } catch (InterruptedException e) {
                    LOGGER.warn(e.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
            result=Prometheus.getMetricsFromProm("http://" + serviceUrl, metricquery, ":30090/api/v1/query?query=");
            try(FileWriter fw = new FileWriter(logFileDir +"/"+  filename)){
                fw.write(result[0].split("value\":\\[")[1].split(",\\\"")[1].split("\\\"")[0]);
            }
            startstring=Prometheus.getMetricsFromProm("http://" + serviceUrl, new String[]{"kube_job_created{namespace=\"job\"}"}, ":30090/api/v1/query?query=")[0];
            startstring=startstring.split("value\"[^\"]*\\\"")[1].split("\\\"")[0];
            endstring=Prometheus.getMetricsFromProm("http://" + serviceUrl, new String[]{"kube_job_status_completion_time{namespace=\"job\"}"}, ":30090/api/v1/query?query=")[0];
            endstring=endstring.split("\"value\":[^\"]*\\\"")[1].split("\\\"")[0];
        }
        for (int i=0;i<metricstring.length;i++){
            metricstring[i]=metricstring[i]+"&start="+startstring+"&end="+endstring+"&step=3";
        }
        boolean erg= Prometheus.writeResultsToCSVFile(Prometheus.splitResults(Prometheus.getMetricsFromProm("http://" + serviceUrl, metricstring, ":30090/api/v1/query_range?query=")),metrics, logFileDir + "/prom_" + filename);

        // Destroy the deployment
        if (!skipDeployment) deleteApp(deployment);
        if(!erg){
            currentLoop--;
            LOGGER.info("Resetting currentLoop to {}", currentLoop);
        }
    }

        private void runWordpressTest() throws IOException {

        InputStream testPlanStream = getClass().getResourceAsStream(jmeterTestplans + "/" + jmeterTestplan);
        InputStream jpegFileStream = getClass().getResourceAsStream(jmeterTestplans + "/wordpress/wal.jpeg");

        // Get HashTree from testplan
        HashTree testPlanTree = SaveService.loadTree(streamToTempFile(testPlanStream, PREFIX, SUFFIX));
        String imageFilePath = streamToTempFile(jpegFileStream, PREFIX, ".jpeg").getAbsolutePath();

        //Set value of the image file variable to the right filepath
        replaceArgumentInHashTree(testPlanTree, "datei", imageFilePath);
        runTest(testPlanTree);
    }

    private void replaceArgumentInHashTree(HashTree hashTree, String argName, String argValue) {
        LOGGER.info("Trying to replace the value of '{}' with '{}'", argName, argValue);

        boolean replaced = false;
        Collection collection = hashTree.values();
        ListedHashTree listedHashTree = (ListedHashTree) collection.iterator().next();
        for (Object o : listedHashTree.list()) {
            if (o instanceof Arguments) {
                Arguments arguments = (Arguments) o;
                for(JMeterProperty jMeterProperty : arguments.getArguments()) {
                    if (jMeterProperty.getName().equalsIgnoreCase(argName)) {
                        Argument argument = (Argument) jMeterProperty.getObjectValue();
                        LOGGER.info("Replaced value of argument '{}': old: {}, new: {}", argName, argument.getValue(), argValue);
                        argument.setValue(argValue);
                        replaced = true;
                    }
                }
            }
        }
        if (!replaced) LOGGER.warn("Argument '{}' not found, ignoring replace", argName);
    }
}
